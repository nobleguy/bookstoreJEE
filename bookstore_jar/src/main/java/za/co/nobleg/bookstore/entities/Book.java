package za.co.nobleg.bookstore.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity(name = "books")
@Cacheable(true)
@Access(AccessType.FIELD)
public class Book extends BaseEntity {
	
	 /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The title name. */
    @Column(length = 255)
    private String title = null;
    
    /** The category name. */
    @Column(length = 255)
    private String category = null;
    
    /** The price. */
    private Double price = null;
    
    @OneToMany(orphanRemoval = false, fetch = FetchType.LAZY, cascade = CascadeType.MERGE, mappedBy = "book")
    private List<Author> authors = new ArrayList<Author>();

    @OneToOne(orphanRemoval = false, fetch = FetchType.LAZY, cascade = CascadeType.MERGE, mappedBy = "book")
    private BookImage  image = new BookImage();
    /** The year . */
  
    private Integer year = null;

    public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
	public List<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}

	public BookImage getImage() {
		return image;
	}

	public void setImage(BookImage image) {
		this.image = image;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public Book clone() {
		return new Book(title, category, price, authors, image , year );
	}

	public Book() {}
	public Book(String title, String category, Double price, List<Author> authors, BookImage image, Integer year) {
		super();
		this.title = title;
		this.category = category;
		this.price = price;
		this.authors = authors;
		this.image = image;
		this.year = year;
	}
	

}
