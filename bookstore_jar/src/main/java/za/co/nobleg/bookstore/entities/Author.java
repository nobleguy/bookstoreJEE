package za.co.nobleg.bookstore.entities;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author Tshepo.Moganedi
 *
 */
@Entity(name = "authors")
@Cacheable(true)
@Access(AccessType.FIELD)
public class Author extends BaseEntity {
	
	 /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
	
	/** The customer name. */
    @Column(length = 255)
    private String name = null;
    
    @ManyToOne
    @JoinColumn(name="book_id", nullable=false)
    private Book book;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

}
