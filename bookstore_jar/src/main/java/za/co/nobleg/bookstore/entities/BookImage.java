package za.co.nobleg.bookstore.entities;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * @author Tshepo.Moganedi
 *
 */
@Entity(name = "bookimage")
@Cacheable(true)
@Access(AccessType.FIELD)
public class BookImage extends BaseEntity {
	
	  private static final long serialVersionUID = 1L;
	  @Column(length = 255)
	  private String imageName;
	  
	  @Column(unique = false, nullable = true, length = 100000)
	  private byte[] image;
	  
	  @OneToOne
	  @JoinColumn(name="book_id", nullable=false)
	  private Book book;


	  public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	
	  public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	

}
