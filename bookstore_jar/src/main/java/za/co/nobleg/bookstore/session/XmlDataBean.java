package za.co.nobleg.bookstore.session;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import za.co.nobleg.bookstore.entities.Author;
import za.co.nobleg.bookstore.entities.Book;
import za.co.nobleg.bookstore.entities.BookImage;
import za.co.nobleg.bookstore.facade.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


import java.io.File;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.Element;



@Startup
@Singleton
public class XmlDataBean {
	
	/**
     * The Constant log.
     */
  

    /**
     * The persistence.
     */
	@EJB
	 private PersistenceFacadeBean persistence;
	
	
	 @Lock(LockType.READ)
	 @PostConstruct
	 public void readXMLDataIntoDatabase () 
	 {
		 try {
			 
			 File inputFile = new File("C:\\Users\\Tshepo.Moganedi\\eclipse-workspace\\bookstoreAst\\bookstore\\bookstore_jar\\seeds\\bookstore.xml");
	         DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	         Document doc = dBuilder.parse(inputFile);
	         doc.getDocumentElement().normalize();
	         System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
	         NodeList nList = doc.getElementsByTagName("book");
	         
	        
	         
	         
	         for (int temp = 0; temp < nList.getLength(); temp++) {
	            Node nNode = nList.item(temp);
	            System.out.println("\nCurrent Element :" + nNode.getNodeName());
	            Book book  = new Book();
		         
		         BookImage bookimage = new BookImage();
		         List<Author> authors = new ArrayList<Author>();
	            
	            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	            	 Element eElement = (Element) nNode;
	            	
	            	System.out.println("Category element :" + eElement.getAttribute("category"));
	            	System.out.println("Title element :" + eElement.getElementsByTagName("title").item(0).getTextContent());
	            	System.out.println("Price element :" + eElement.getElementsByTagName("price").item(0).getTextContent());
	            	System.out.println("Year element :" + eElement.getElementsByTagName("year").item(0).getTextContent());
	            	book.setCategory(eElement.getAttribute("category"));
	            	book.setTitle(eElement.getElementsByTagName("title").item(0).getTextContent());
	            	book.setPrice( Double.parseDouble(eElement.getElementsByTagName("price").item(0).getTextContent()));
	            	book.setYear(Integer.parseInt(eElement.getElementsByTagName("year").item(0).getTextContent()));
	            	
	            	NodeList authListElement  =  ((Element)eElement.getElementsByTagName("authors").item(0)).getElementsByTagName("author"); 
	            	for (int tempAuth = 0; tempAuth < authListElement.getLength(); tempAuth++) {
	            		 Node nAuthNode = authListElement.item(tempAuth);
	            		 if( nAuthNode.getNodeType() == Node.ELEMENT_NODE) {
	            			 System.out.println("Year element:" + nAuthNode.getTextContent());
	            			 Author author = new Author();
	            			 author.setName(nAuthNode.getTextContent());
	            			 author.setBook(book);
	            			 authors.add(author);
	            		 }
	
	            	}
	            	bookimage.setImageName("test");
	            	bookimage.setBook(book);
	            	book.setImage(bookimage);
	            	book.setAuthors(authors);
	            	persistence.saveOrUpdate(book);
	            	
	            }
	         }
	         
	         
			 
		 }
		 catch (Exception e) {
	         e.printStackTrace();
	      }
		 	finally {
			 
		 }
		 
		 
		 
	 }

}
