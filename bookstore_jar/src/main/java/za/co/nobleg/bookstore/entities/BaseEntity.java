package za.co.nobleg.bookstore.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

// TODO: Auto-generated Javadoc
/**
 * The type Base entity.
/**
 * @author Tshepo.Moganedi
 *
 */
@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class BaseEntity implements Serializable
{
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id = null;

    /**
     * The created_at.
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date created_at = new Date();

    /**
     * The updated_at.
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated_at = new Date();

    /**
     * Instantiates a new base entity.
     */
    public BaseEntity()
    {
        setCreated_at(new Date());
        setUpdated_at(getCreated_at());
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Gets created at.
     *
     * @return the created at
     */
    public Date getCreated_at()
    {
        return created_at;
    }

    /**
     * Sets created at.
     *
     * @param created_at the created at
     */
    public void setCreated_at(Date created_at)
    {
        this.created_at = created_at;
    }

    /**
     * Gets updated at.
     *
     * @return the updated at
     */
    public Date getUpdated_at()
    {
        return updated_at;
    }

    /**
     * Sets updated at.
     *
     * @param updated_at the updated at
     */
    public void setUpdated_at(Date updated_at)
    {
        this.updated_at = updated_at;
    }
}
