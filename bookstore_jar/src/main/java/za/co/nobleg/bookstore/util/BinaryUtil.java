package za.co.nobleg.bookstore.util;

import org.apache.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

// TODO: Auto-generated Javadoc

/**
 * A utility class for binary operations.
 *
 * @author Navaneeth Sen
 * @since 2016 /10/31
 */
public class BinaryUtil
{

    /**
     * The Constant log.
     */
    private final static Logger log = Logger.getLogger(BinaryUtil.class);

    /**
     * Marshall byte [ ].
     *
     * @param <T> the type parameter
     * @param obj the obj
     * @return the byte [ ]
     * @throws IOException the io exception
     */
    public static <T extends Serializable> byte[] marshall(T obj) throws IOException
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(obj);
        oos.close();
        return baos.toByteArray();
    }

    /**
     * Job id from reference.
     *
     * @param jobRef the job ref
     * @return the long
     */
    public static Long jobIDFromReference(String jobRef)
    {
        return Long.valueOf(jobRef.substring(2, jobRef.length()));
    }

    /**
     * Job reference from id.
     *
     * @param id the id
     * @return the string
     */
    public static String jobReferenceFromID(Long id)
    {
        return String.format("RR%010d", id);
    }

    /**
     * Unmarshall t.
     *
     * @param <T> the type parameter
     * @param b   the b
     * @param cl  the cl
     * @return the t
     * @throws IOException            the io exception
     * @throws ClassNotFoundException the class not found exception
     */
    public static <T extends Serializable> T unmarshall(byte[] b, Class<T> cl) throws IOException, ClassNotFoundException
    {
        ByteArrayInputStream bais = new ByteArrayInputStream(b);
        ObjectInputStream ois = new ObjectInputStream(bais);
        Object o = ois.readObject();
        return cl.cast(o);
    }

    /**
     * Marshall output stream.
     *
     * @param <T> the type parameter
     * @param obj the obj
     * @param os  the os
     * @return the output stream
     * @throws IOException the io exception
     */
    public static <T extends Serializable> OutputStream marshall(T obj, OutputStream os) throws IOException
    {
        ObjectOutputStream oos = new ObjectOutputStream(os);
        oos.writeObject(obj);
        oos.close();
        return os;
    }

    /**
     * To string list.
     *
     * @param any the any
     * @return the list
     */
    public static List<String> toStringList(List<Object> any)
    {
        List<String> list = new ArrayList<String>();
        for (Object o : any)
        {
            list.add(o != null ? o.toString() : null);
        } // for

        return list;
    }

    /**
     * To long list.
     *
     * @param any the any
     * @return the list
     */
    public static List<Long> toLongList(List<Object> any)
    {
        List<Long> list = new ArrayList<Long>();
        for (Object o : any)
        {
            list.add(o != null ? Long.valueOf(o.toString()) : null);
        } // for

        return list;

    }

    /**
     * Unmarshall t.
     *
     * @param <T> the type parameter
     * @param is  the is
     * @param cl  the cl
     * @return the t
     * @throws IOException            the io exception
     * @throws ClassNotFoundException the class not found exception
     */
    public static <T extends Serializable> T unmarshall(InputStream is, Class<T> cl) throws IOException, ClassNotFoundException
    {
        ObjectInputStream ois = new ObjectInputStream(is);
        Object o = ois.readObject();
        return cl.cast(o);
    }

    /**
     * Clamp.
     *
     * @param value the value
     * @param min   the min
     * @param max   the max
     * @return the float
     */
    public static float clamp(float value, float min, float max)
    {
        value = (value < min ? min : value);
        return (value > max ? max : value);
    }

    /**
     * Human readable file size from bytes.
     *
     * @param length the length
     * @return the string
     */
    public static String humanReadableFileSizeFromBytes(long length)
    {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setGroupingUsed(true);
        nf.setMinimumFractionDigits(0);
        nf.setMaximumFractionDigits(2);

        // GB
        double gb = (double) length / (1024.0d * 1024.0d * 1024.0d);
        if (gb > 1)
        {
            return nf.format(gb) + "GB";
        }

        // Mb
        double mb = (double) length / (1024.0d * 1024.0d);
        if (mb > 1)
        {
            return nf.format(mb) + "MB";
        }

        // Kb
        double kb = (double) length / (1024.0d);
        if (kb > 1)
        {
            return nf.format(kb) + "KB";
        }

        return length + " bytes";

    }

    /**
     * Elapsed time.
     *
     * @param time the time
     * @return the string
     */
    public static String elapsedTime(long time)
    {

        DateFormat df2 = new SimpleDateFormat("HH:mm:ss.SSS");
        df2.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));

        Calendar calendar = GregorianCalendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTimeInMillis(time);

        return df2.format(calendar.getTime());

    }

    /**
     * Human readable file size from bytes.
     *
     * @param length the length
     * @return the string
     */
    public static String hrFileSize(long length)
    {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setGroupingUsed(true);
        nf.setMinimumFractionDigits(0);
        nf.setMaximumFractionDigits(2);

        // GB
        double gb = (double) length / (1024.0d * 1024.0d * 1024.0d);
        if (gb > 1)
        {
            return nf.format(gb) + "GB";
        }

        // Mb
        double mb = (double) length / (1024.0d * 1024.0d);
        if (mb > 1)
        {
            return nf.format(mb) + "MB";
        }

        // Kb
        double kb = (double) length / (1024.0d);
        if (kb > 1)
        {
            return nf.format(kb) + "KB";
        }

        return length + " bytes";

    }

    /**
     * Matches enum.
     *
     * @param cls       the cls
     * @param enumValue the enum value
     * @return true, if successful
     */
    public static boolean matchesEnum(Class cls, String enumValue)
    {
        try
        {
            Field f = cls.getField(enumValue);
            return f.isEnumConstant();
        }
        catch (Exception ex)
        {
            // NOT IMPORTANT
            // log.trace(ex.getMessage(), ex);
        } // catch

        return false;
    }

    /**
     * Gets enum values.
     *
     * @param cls the cls
     * @return the enum values
     */
    public static List<String> getEnumValues(Class cls)
    {
        List<String> vals = new ArrayList<>();

        Object[] enumConstants = cls.getEnumConstants();
        for (Object enumVal : enumConstants)
        {
            vals.add(enumVal.toString());
        }
        return vals;
    }

    /**
     * Strip stack trace.
     *
     * @param ex the ex
     * @return the string
     */
    public static String stripStackTrace(Exception ex)
    {
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        return sw.toString();

    }

}
