/*
 * 
 */
package za.co.nobleg.bookstore.facade;

import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import za.co.nobleg.bookstore.util.BinaryUtil;
import za.co.nobleg.bookstore.entities.BaseEntity;
/**
 * The Class PersistenceFacadeImpl.
 */
@Stateless
public class PersistenceFacadeBean
{

    /**
     * The Constant log.
     */
    private static final Logger log = Logger.getLogger(PersistenceFacadeBean.class);

    /**
     * The em.
     */
    @PersistenceContext(unitName = "bookstore_pu")
    private EntityManager em;

    /*
     * (non-Javadoc)
     * @see
     * za.co.nobleg.projectlens.facade.PersistenceFacade#saveOrUpdate
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public <T extends BaseEntity> T saveOrUpdate(T entity)
    {

        long time = System.currentTimeMillis();

        try
        {
            if (entity.getCreated_at() == null)
            {
                entity.setCreated_at(new Date());
            } // if
            entity.setUpdated_at(new Date());

            log.trace("saveOrUpdate:-- START: entity: " + entity.getClass().getName());
            entity = em.merge(entity);
            em.flush();

            return entity;
        }
        catch (Exception ex)
        {
            log.error(ex.getMessage(), ex);
            throw new RuntimeException(ex.getMessage(), ex);
        }
        finally
        {
            log.trace("saveOrUpdate(" + entity.getClass().getSimpleName() + ") took: " + (BinaryUtil.elapsedTime(System.currentTimeMillis() - time)));
        } // finally
    }

    /*
     * (non-Javadoc)
     * @see
     * za.co.nobleg.projectlens.facade.PersistenceFacade#retrieveHQL
     */
    public <T extends BaseEntity> List<T> retrieveHQL(Class<T> cls, String hql, String[] parameters, Object[] values, Integer maxResults)
    {

        long time = System.currentTimeMillis();
        try
        {

            log.trace("fetch type: " + cls.getName() + ", hql: " + hql + ", maxResults: " + maxResults);

            if (parameters != null)
            {
                log.trace("\t parameters: " + Arrays.asList(parameters));
                log.trace("\t values: " + Arrays.asList(values));
            }

            List<T> result;

            Query q = em.createQuery(hql);
            if (maxResults != null && maxResults.intValue() > 0)
            {
                q.setMaxResults(maxResults);
            } // if

            if (parameters != null)
            {
                for (int i = 0; i < parameters.length; i++)
                {
                    q.setParameter(parameters[i], values[i]);
                }
            }

            result = q.getResultList();

            log.trace("\t Returning " + result.size() + " of type: " + cls.getName());

            return result;

        }
        finally
        {
            String h2 = (hql.length() > 25 ? hql.substring(0, 20) : hql);

            log.trace("retrieveHQL:-- DONE, slot: " + h2 + ", took: " + (BinaryUtil.elapsedTime(System.currentTimeMillis() - time)));
        } // finally

    }

    /*
     * (non-Javadoc)
     * @see
     * za.co.nobleg.projectlens.facade.PersistenceFacade#retrieveByID
     */
    public <T extends BaseEntity> T retrieveByID(Class<T> cls, Object id)
    {
        try
        {
            log.trace("retrieveByID:-- START: cls: " + cls.getName() + ", id: " + id);

            T entity = em.find(cls, id);
            log.trace("\t entity(id:" + id + ") retrieved: " + entity);
            return entity;
        }
        catch (Exception ex)
        {
            log.error(ex.getMessage(), ex);
            throw new RuntimeException(ex.getMessage(), ex);
        } // catch
    }

    /*
     * (non-Javadoc)
     * @see
     * za.co.nobleg.projectlens.facade.PersistenceFacade#updateNativeSQL
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int updateNativeSQL(String sql, String[] parameters, Object[] values)
    {

        long time = System.currentTimeMillis();

        log.trace("updateNativeSQL:-- START, sql: " + sql);

        if (parameters != null)
        {
            log.trace("\t parameters: " + Arrays.asList(parameters));
            log.trace("\t values: " + Arrays.asList(values));
        } // if

        Query q = em.createNativeQuery(sql);

        if (parameters != null)
        {
            for (int i = 0; i < parameters.length; i++)
            {
                q.setParameter(parameters[i], values[i]);
            } // for
        } // if

        int cnt = q.executeUpdate();

        log.trace("\t updateNativeSQL affected: " + cnt + " records, took: " + BinaryUtil.elapsedTime(System.currentTimeMillis() - time));
        return cnt;

    }

    /*
     * (non-Javadoc)
     * @see
     * za.co.nobleg.projectlens.persistence.RecordingPOCPersistenceBean#
     * retrieveHQL(java.lang.Class, java.lang.String, java.util.Map)
     */
    public <T extends BaseEntity> List<T> retrieveHQL(Class<T> cls, String hql, Map<String, Object> parameters)
    {
        return retrieveHQL(cls, hql, parameters.keySet().toArray(new String[0]), parameters.values().toArray(), null);
    }


    public int updateNativeSQL(String sql)
    {
        return updateNativeSQL(sql, null, null);
    }
}
