package za.co.nobleg.bookstore.facade;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import za.co.nobleg.bookstore.entities.Book;

@Stateless
public class BookFacadeBean {


    /** The persistence. */
    @EJB
    PersistenceFacadeBean persistence;

	public Book getBookBybyTitleandCategory(String title, String category) {
		
		return null;
	}

	public List<Book> listAllBooks() {
		
		return persistence.retrieveHQL(Book.class, "select f from books f order by created_at", null, null, null);
		
	}

}
