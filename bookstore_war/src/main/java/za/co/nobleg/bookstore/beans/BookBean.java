package za.co.nobleg.bookstore.beans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

import za.co.nobleg.bookstore.entities.Book;
import za.co.nobleg.bookstore.facade.BookFacadeBean;

@ManagedBean(name = "bookBean")
@SessionScoped
public class BookBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Book beforeEditItem = null;
	private Book item = new Book();
	private boolean edit;
   	
	private List<Book> list;
	
	 @Inject
	 private BookFacadeBean clientBookBean;

    @PostConstruct
    public void init() {
       list = clientBookBean.listAllBooks();
    }

    
    public void edit(Book item) {
    	beforeEditItem = item.clone();
        this.item = item;
        edit = true;
    }
	public List<Book> getList() {
		return list;
	}

	public void setList(List<Book> list) {
		this.list = list;
	}


	public Book getItem() {
		return item;
	}


	public void setItem(Book item) {
		this.item = item;
	}

	 public boolean isEdit() {
	        return this.edit;
	    }

}
