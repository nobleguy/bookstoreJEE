package za.co.nobleg.bookstore.restful;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

// TODO: Auto-generated Javadoc

/**
 * The Class STBService.
 */
@Path("/test")
public class TestService
{

    /**
     * The Constant log.
     */
    private final static Logger log = Logger.getLogger(TestService.class);

    /**
     * The persistence.
     */
    // @Inject - Example Bean Injection.
    // PersistenceFacade persistence;

    /**
     * Stbservice stbjson message.
     *
     * @param req the req
     * @param jsonMessage the json message
     * @return the stbjson message
     */
    @GET
    @POST
    @Produces(MediaType.TEXT_HTML)
    public String stbservice(@Context HttpServletRequest req, String testInput)
    {
        try
        {
            log.debug("testservice:-- START, testInput: " + testInput);

            return "<h1>" + testInput + "/h1>";
        } catch (Exception ex)
        {
            log.error(ex.getMessage(), ex);
            return "<h2>" + ex.toString() + "</h2>";
        }

    }

}
