package za.co.nobleg.bookstore.restful;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * The Class RestFulApplication.
 * This is the way jaxb has implemented restful
 */
@ApplicationPath("/testservice")
public class RestFulApplication extends Application
{

}
