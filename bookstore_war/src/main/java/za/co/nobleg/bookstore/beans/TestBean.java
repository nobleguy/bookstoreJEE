package za.co.nobleg.bookstore.beans;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * The Class TestBean.
 */
@ManagedBean(name = "testBean")
@SessionScoped // use session scoped, otherwise your variables will be confuswed
// between users
public class TestBean implements Serializable
{

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The Constant log.
     */
    private final static Logger log = Logger.getLogger(TestBean.class);

    /** The catalog bean. */
    // @Inject - Example Injection
    // private CatalogSummaryBean catalogBean;

    /**
     * The ip.
     */
    private String testString = null;

    /**
     * The port.
     */
    private Integer testNumber = null;

    /**
     * Adds the message to faces context.
     *
     * @param message the message
     */
    public void addMessageToFacesContext(String message)
    {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, "Information."));
    }

    /**
     * Adds the message to faces context.
     *
     * @param ex the ex
     */
    public void addMessageToFacesContext(Exception ex)
    {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), "Error Occured."));
    }

    /**
     * Execute_dec_details.
     *
     * @param actionEvent the action event
     */
    public void testBeanEvent(ActionEvent actionEvent)
    {
        log.debug("In here!");
        addMessageToFacesContext("Bean raised event!");

        this.testString = "test 123";
        this.testNumber = 123;

    }

    /**
     * Gets the test string.
     *
     * @return the testString
     */
    public String getTestString()
    {
        return testString;
    }

    /**
     * Sets the test string.
     *
     * @param testString the testString to set
     */
    public void setTestString(String testString)
    {
        this.testString = testString;
    }

    /**
     * Gets the test number.
     *
     * @return the testNumber
     */
    public Integer getTestNumber()
    {
        return testNumber;
    }

    /**
     * Sets the test number.
     *
     * @param testNumber the testNumber to set
     */
    public void setTestNumber(Integer testNumber)
    {
        this.testNumber = testNumber;
    }

}
