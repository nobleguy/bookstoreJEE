Creating Archetypes: https://maven.apache.org/guides/mini/guide-creating-archetypes.html
ArcheTypes from project: http://maven.apache.org/archetype/maven-archetype-plugin/advanced-usage.html
MultiModule Archetype Creation: http://maven.apache.org/archetype/maven-archetype-plugin/examples/create-multi-module-project.html


Create and install PrimefacesArcheType from Git Repository: 
git clone ssh://[user]@192.168.250.203/git/tools_repositories/PrimefacesTemplate
cd PrimefacesTemplate\target\generated-sources\archetype
mvn install


Create Primefaces project from maven archetype, with Arquillian unit test integration, Full JEE7 support, and JSF 2.2 with EJB 3.2 and JPA 2.2
First install the Primefaces archetype from git: https://btd-softwareservices.slack.com/archives/C53EMTEUV/p1493028766649583
Create new empty project folder, outside of the archetype project you just checked out, in fact, delete the archetype folder you just cehcked out as you never need it again until it changes, then cd into your newly created project folder then run:
mvn archetype:generate -DarchetypeCatalog=local

select local, and enter the archetype info you want.

here is an example of output.

to update the archetype (this project):
mvn archetype:create-from-project

then commit to git.
